create database if not exists Aprendyx;
use Aprendyx;

create table if not exists espanolPortuges(
id_palabra int primary key auto_increment not null,
palabra_espanol varchar(100),
palabra_portu varchar(100)
)Engine InnoDB;

insert into espanolPortuges(palabra_portu,palabra_espanol) value('mulher','mujer'),('borboleta','mariposa'),('batata','papa');
select * from espanolPortuges;
delete from espanolPortuges where id_palabra =4; 

create table if not exists jugar(
id_juego int auto_increment not null primary key,
id_palabra int,
palabra_juego varchar(100),
constraint fk_id_palabra foreign key (id_palabra) references espanolPortuges(id_palabra) on update cascade on delete cascade
)Engine InnoDB;

insert into jugar(id_palabra,palabra_juego) value(1,'mujer'), (2,'borboleta');

-- español a protugues 1 --
select j.id_juego, p.palabra_espanol, j.palabra_juego from jugar j
inner join espanolPortuges p on j.id_palabra = p.id_palabra;

-- portugues a español 2 --
select j.id_juego, p.palabra_portu, j.palabra_juego from jugar j
inner join espanolPortuges p on j.id_palabra = p.id_palabra;
