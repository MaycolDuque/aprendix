/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 *
 * @author Maycol Duque
 */
public class Conexion {
    private static String  bd ="Aprendyx";
    private static String pass="root";
    private static String user = "root";
    private static String ruta ="jdbc:mysql://localhost:3306/"+ bd+"?useSSL=false";
    
    private Connection cxn= null;

    public Connection getCxn() {
        return cxn;
    }

    public void setCxn(Connection cxn) {
        this.cxn = cxn;
    }
    public void Conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.cxn =DriverManager.getConnection(ruta, user, pass);
            if (this.cxn !=null) {
                System.out.println("Conecto a: "+bd);   
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error al conectarce: "+e.getMessage());
        }
    }
    public void desconectar() throws Exception{
        try {
            if (this.cxn !=null) {
                if(this.cxn.isClosed() != true){
                this.cxn.close();
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
